ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=latest

FROM registry:2.8.1 as registry

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ARG USER=docker

COPY config/config.yml /etc/docker/registry/config.yml
COPY --from=registry /bin/registry /bin/registry
COPY scripts /oscap-fixes/

RUN mkdir -p /etc/docker/registry && \
    mkdir -p /var/lib/registry && \
    groupadd -g 1000 ${USER} && \
    useradd -r -u 1000 -m -s /sbin/nologin -g ${USER} ${USER} && \
    chown ${USER}:0 /etc/docker/registry && \
    chown ${USER}:0 /var/lib/registry && \ 
    chown ${USER}:0 ${HOME} && \
    chmod g=u ${HOME} && \
    dnf update -y && dnf -y upgrade && \
    dnf install -y httpd-tools && \
    /oscap-fixes/xccdf_org.ssgproject.content_rule_file_permissions_binary_dirs.sh && \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
    chmod 755 /bin/registry

HEALTHCHECK --interval=10s --timeout=5s --start-period=1m --retries=5 CMD curl -I -f --max-time 5 http://localhost:5000 || exit 1

VOLUME ["/var/lib/registry"]

EXPOSE 5000

USER ${USER}

ENTRYPOINT ["registry"]

CMD ["serve", "/etc/docker/registry/config.yml"]
