# Docker Registry
This image contains an implementation of the Docker Registry HTTP API V2 for use with Docker 1.6+. See github.com/docker/distribution for more details about what it is.

## Run a local registry: Quick Version
```
$ docker run -d -p 5000:5000 --restart always --name registry registry:2
```

Now, use it from within Docker:

```
$ docker pull ubuntu
$ docker tag ubuntu localhost:5000/ubuntu
$ docker push localhost:5000/ubuntu
```
## Recommended Reading
The documentation over at docs.docker.com is a good place to learn more about what the registry is, how it works, and how to use it.

Specifically, the section regarding deployment has pointers for more complex use cases than simply running a registry on localhost.

# Support
If you are interested in commercial support, the Docker Trusted Registry provides an image registry, LDAP/Active Directory integration, security certificates, and more in a solution that includes commercial support.

# License
View license information for the software contained in this image.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

Some additional license information which was able to be auto-detected might be found in the repo-info repository's registry/ directory.

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.